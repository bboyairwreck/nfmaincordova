$(document).ready(function() {
    document.addEventListener("deviceready", loadOpenTok, false);

    function loadOpenTok() {
        // Sign up for an OpenTok API Key at: https://tokbox.com/signup
        // Then generate a sessionId and token at: https://dashboard.tokbox.com
        var apiKey = "45244792"; // INSERT YOUR API Key
        var sessionId = "1_MX40NTI0NDc5Mn5-MTQzMjY4MDQyMTE3MX4zQ0NFTDJjcDR1QW9NRFozSk5IejZWSml-fg";
        var token = "T1==cGFydG5lcl9pZD00NTI0NDc5MiZzaWc9ZDNkZjUxZTc1MWViNDAxOGQ2Yzc3ZWY0NDJhYjYxYzk3NGM0M2QwZDpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTFfTVg0ME5USTBORGM1TW41LU1UUXpNalk0TURReU1URTNNWDR6UTBORlRESmpjRFIxUVc5TlJGb3pTazVJZWpaV1NtbC1mZyZjcmVhdGVfdGltZT0xNDMyNjgwNDI1Jm5vbmNlPTAuMDM0NzUwMzkzMDA3ODE5MjM2JmV4cGlyZV90aW1lPTE0MzUyNzE3NjQmY29ubmVjdGlvbl9kYXRhPQ==";


        //var publisher = TB.initPublisher(apiKey, 'patVideoWrap');
        var publisher = TB.initPublisher(apiKey, 'patVideoWrap', {width:320, height:240});
        var session = TB.initSession(apiKey, sessionId);

        // Subscribe to streams that come from other users publishing
        session.on({
            'streamCreated': function (event) {

                var $compVideoWrap = $("#compVideoWrap");
                var vidWidth = parseInt($compVideoWrap.width());
                var vidHeight = parseInt($compVideoWrap.height());
                session.subscribe(event.stream, "compVideoWrap", {
                    width: vidWidth,
                    height: vidHeight,
                    subscribeToAudio: true});
                TB.updateViews();
            },
            'streamDestroyed': function (event) {
                session.disconnect();
            },
            'sessionDisconnected' : function (event) {
                localStorage.removeItem("callID");
                navWithParams("action.html");
                window.location = "action.html";
            },
            'sessionConnected' : function (event) {
                localStorage.setItem("callID", event.stream.streamId);
            }
        });

        // Establish OpenTok session
        session.connect(token, function (error) {
            if (error) {
                alert(error.message);
            } else {
                //session.publish( 'patVideoWrap', {width:320, height:240} );
                session.publish(publisher);
                TB.updateViews();
                $("#patVideoWrap").css("opacity", 1);
            }
        });

        $("#endCall").on("touchend", function(){
            session.disconnect();
        });

    }

    function navWithParams(pageHref) {

        var targetElement = document.getElementById('ghost');

        if (targetElement == null) {
            alert("ghost anchor does not exist");
        }

        targetElement.href = pageHref;

        var evt = document.createEvent('UIEvent');
        evt.initUIEvent('touchend', true, true, window, 1);
        targetElement.dispatchEvent(evt);
    }

});