$(document).ready(function() {


    $( ".triggerNotification" ).click(function() {
      alert( "fuck yeah" );
    });


    // Firebase - Check for changes
    var fire = new Firebase('https://neverforgotten.firebaseio.com/');
    var patID = localStorage.getItem("patient");
    var patFire = fire.child(patID);
    patFire.on('child_changed', fbSettingChanged);
    patFire.on('child_added', fbSettingChanged);

    var createReminderButton = localStorage.getItem("createReminderButton");
    var callButton = localStorage.getItem("callButton");
    var remindersLayout = localStorage.getItem("layout");

    var $viewReminder = $('#aViewRemindersBtn');
    var $createBtn = $("#aCreateBtn");
    var $callBtn = $("#aCallBtn");

    if (createReminderButton == 1) {
        $createBtn.css("display", "block");
    }

    if (callButton == 1) {   // call button only
        $callBtn.css("display", "block");
    }

    if (localStorage.getItem("greetingSound") == 1) {
        speakPhrase("What would you like to do?");
    }
});


function fbSettingChanged(snapshot) {
    var value = snapshot.val();
    var key = snapshot.key();

    var $createBtnLg = $("#aCreateBtnLg");
    var $createBtnSm = $("#aCreateBtnSm");
    var $callBtnLg = $("#aCallBtnLg");
    var $callBtnSm = $("#aCallBtnSm");

    if (key == "CreateReminderButton") {
        if (value == 0) {       // no create reminder button
            $createBtnLg.css("display", "none");
            $createBtnSm.css("display", "none");
            if (localStorage.getItem("callButton") == 1) {
                $callBtnLg.css("display", "block");
                $callBtnSm.css("display", "none");
            } else {
                $callBtnLg.css("display", "none");
                $callBtnSm.css("display", "none");
            }
        } else {
            if (localStorage.getItem("callButton") == 0) {
                $createBtnLg.css("display", "block");
                $createBtnSm.css("display", "none");
                $callBtnLg.css("display", "none");
                $callBtnSm.css("display", "none");
            } else {
                $createBtnLg.css("display", "none");
                $createBtnSm.css("display", "block");
                $callBtnLg.css("display", "none");
                $callBtnSm.css("display", "block");
                $createBtnSm.css("float", "left");
                $callBtnSm.css("float", "right");
            }
        }

        localStorage.setItem("createReminderButton", value);

    } else if (key == "CallButton") {
        if (value == 0) {
            $callBtnLg.css("display", "none");
            $callBtnSm.css("display", "none");
            if (localStorage.getItem("createReminderButton") == 1) {
                $createBtnLg.css("display", "block");
                $createBtnSm.css("display", "none");
            } else {
                $createBtnLg.css("display", "none");
                $createBtnSm.css("display", "none");
            }
        } else {
            if (localStorage.getItem("createReminderButton") == 0) {
                $callBtnLg.css("display", "block");
                $callBtnSm.css("display", "none");
                $createBtnLg.css("display", "none");
                $createBtnSm.css("display", "none");
            } else {
                $callBtnLg.css("display", "none");
                $callBtnSm.css("display", "block");
                $createBtnLg.css("display", "none");
                $createBtnSm.css("display", "block");
                $createBtnSm.css("float", "left");
                $callBtnSm.css("float", "right");
            }
        }
        localStorage.setItem("callButton", value);
    } else if (key == "CalendarLayout") {
        var $viewReminder = $('#aViewRemindersBtn');
        var $viewCalendar = $('#aViewCalendarBtn');

        if (value == 0) {
            $viewReminder.css("display", "block");
            $viewCalendar.css("display", "none");
            localStorage.setItem("layout", "List");
        } else {
            $viewReminder.css("display", "none");
            $viewCalendar.css("display", "block");
            localStorage.setItem("layout", "Calendar");
        }
    }
}

