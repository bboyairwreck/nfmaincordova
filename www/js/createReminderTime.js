!(function(){
    var eventName = "";
    var eventDate = "";
    var hours = null;
    var minutes = null;
    var timeInterval = null;

    $(document).ready(function() {
        myParams = getParams();

        if (myParams != null) {
            eventName = myParams["eventName"];
            eventDate = myParams["eventDate"];
            var eventDateStringArr = dateFormat(eventDate);
            var year = parseInt(eventDateStringArr["year"]) % 100;
            var month = eventDateStringArr["month"];
            var day = eventDateStringArr["day"];
        }
        var now = new Date();
        hours = now.getHours() % 12;
        minutes = now.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (hours == 0) {
            hours = 12;
        }
        var time = hours + ":" + minutes;
        var timeString = timeFormat(time);
        var amPm = timeString.slice(-2);
        if (amPm == "AM") {
            $("#amButton").attr("src", "img/am_yellow.png");
        } else {
            $("#pmButton").attr("src", "img/pm_yellow.png");
        }

        $("#hourNum").text(hours);
        $("#minuteNum").text(minutes);

        $("#preview").text(eventName + ", " + month + "/" + day + "/" + year);
    });

    function addHour() {
        if (hours == 12) {
            hours = 1;
        } else {
            hours++;
        }
        $("#hourNum").text(hours);
    }

    function minusHour() {
        if (hours == 1) {
            hours = 12;
        } else {
            hours--;
        }
        $("#hourNum").text(hours);
    }

    function addMin() {
        if (minutes == 59) {
            minutes = 0;
        } else {
            minutes++;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        $("#minuteNum").text(minutes);
    }

    function minusMin() {
        if (minutes == 0) {
            minutes = 59;
        } else {
            minutes--;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        $("#minuteNum").text(minutes);
    }

    $("#addHour").on("touchstart", function() {
        timeInterval = setInterval(function() {
            addHour();
        }, 300);
    });

    $("#addHour").on("touchend", function() {
        clearInterval(timeInterval);
        addHour();
    });

    $("#minusHour").on("touchstart", function() {
        timeInterval = setInterval(function() {
            minusHour();
        }, 300);
    });

    $("#minusHour").on("touchend", function() {
        clearInterval(timeInterval);
        minusHour();
    });

    $("#addMin").on("touchstart", function() {
        timeInterval = setInterval(function() {
            addMin();
        }, 300);
    });

    $("#addMin").on("touchend", function() {
        clearInterval(timeInterval);
        addMin();
    });

    $("#minusMin").on("touchstart", function() {
        timeInterval = setInterval(function() {
            minusMin();
        }, 300);
    });

    $("#minusMin").on("touchend", function() {
        clearInterval(timeInterval);
        minusMin();
    });

    $("#amButton").on("touchend", function() {
        $("#pmButton").attr('src', 'img/pm_grey.png');
        $(this).attr('src', 'img/am_yellow.png');
        if ($("#hourNum").text() != "") {
            if (hours == 12) {
                hours = 0;
            }
        }
    });

    $("#pmButton").on("touchend", function() {
        $("#amButton").attr('src', 'img/am_grey.png');
        $(this).attr('src', 'img/pm_yellow.png');
        if ($("#hourNum").text() != "") {
            if (hours != 12) {
                hours = hours + 12;
            }
        }
    });

    $("#nextTime").click(function(){
        // ensure Event Name has something
        if (hours != null && minutes != null) {

            // grab event time
            var evTime = "";
            if (hours < 10) {
                evTime = "0" + hours + ":" + minutes;
            } else {
                evTime = hours + ":" + minutes;
            }

            // Event time in param
            var eventInfo = [];
            eventInfo["eventName"] = eventName;
            eventInfo["eventDate"] = eventDate;
            eventInfo["eventTime"] = evTime;
            params.push(eventInfo);

            // navigate to next to createDate
            var href = $(this).data("href");    // page location
            navWithParams(href);
        } else {
            alert("Please enter a time before continuing");
        }

    });

    $("#backDate").click(function() {
        params.pop();
        var href = $(this).data("href");
        navWithParams(href);
    });

}());


