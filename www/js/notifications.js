!(function() {
    document.addEventListener("deviceready", loadNotif, false);
    document.addEventListener("deviceready", loadCallFirebase, false);

    $(document).ready(function(){

        // Firebase - Check for changes
        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patID = localStorage.getItem("patient");
        var patFire = fire.child(patID + "/reminderCreatedComp");

        patFire.on('child_added', fbRemAdded);
    });


    $(".blackShade, .notifDismiss").on("touchend", function() {
        $(".blackShade").fadeOut();
        $(".blackShade").removeClass("open");

        var $notifModal = $(".notificationModal");
        if ($notifModal.hasClass("open")){
            $notifModal.removeClass("open");
        }

        $notifModal.fadeOut();
    });


    function fbRemAdded(snapshot) {

        var msg = snapshot.val();
        var key = snapshot.key();

        //alert("msg : " + msg + "; key = " + key);

        var patID = localStorage.getItem("patient");
        var eventID = msg["eventID"];
        var fNameComp = msg["firstNameComp"];
        var lNameComp = msg["lastNameComp"];

        var url = "http://ericchee.com/neverforgotten/getEventInfo.php";
        $.ajax(url, {
            dataType : "json",
            data : {
                'n' : patID,
                'id' : eventID
            },
            success : reminderInfoSuccess,
            error : ajaxErrorRem
        });

    }

    function reminderInfoSuccess(data) {
        var eventName = data["EventTitle"];
        var eventDateTime = data["EventDateTime"];
        var eventDateTimeArr = datetimeFormat(eventDateTime);
        var eventDate  = eventDateTimeArr["date"];
        var eventDateFormatted = dateFormat(eventDate);
        var eventTime = eventDateTimeArr["time"];
        var eventTimeFormatted = timeFormat(eventTime);

        var $notifModal = $(".notificationModal.created");
        $(".notifTitle").text(eventName);
        $(".notifTime").text(eventTimeFormatted);
        // TODO use something with eventDateFormatted which is an array

        $notifModal.addClass("open");

        var $blackShade = $(".blackShade");
        $blackShade.fadeIn();

        $notifModal.fadeIn();

        // remove from call log if received
        clearRemAddedLog();
    }

    function clearRemAddedLog(){

        // remove from call log if received
        var patID = localStorage.getItem("patient");

        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patFire = fire.child(patID + "/reminderCreatedComp");
        patFire.set(null);
    }

    function ajaxErrorRem( xhr, status, errorThrown ) {
        alert( "Sorry, there was an ajax problem with new reminder notification!" );
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function loadNotif() {

        // ajax to get Reminders of Patient
        var url = "http://ericchee.com/neverforgotten/getReminders_Patient.php";

        $.ajax(url, {
            dataType: "json",
            data: {
                'n': localStorage.getItem("patient")
            },
            success: receivedAllRemindersOfPatient,
            error: ajaxError
        });

        // Show Notification Modal on receive
        cordova.plugins.notification.local.on("trigger", function(notification) {
            //alert("triggered: " + notification.id);
            var dataJSON = JSON.parse(notification.data);

            var datetimeArr = datetimeFormat(dataJSON["eventTime"]);    // [time, date]
            var curEventID = dataJSON["eventID"];
            var curEventTitle = dataJSON["eventTitle"];

            var curTime = datetimeArr["time"];          // 03:05:00
            var curTimeString = timeFormat(curTime);    // 3:05 AM

            var curDate = datetimeArr["date"];          // 2015-20-12

            var curDateFormatArr = dateFormat(curDate); // [year, month, monthName, day, dateLine]

            var dayString = curDateFormatArr["dayName"];

            if (getCurDateString() == curDateFormatArr["dateLine"]) {
                dayString = "Today";
            }
            //var $notifModalWrap = $("#notificationModalWrap");
            //$("#notificationModalWrap h1").text(dataJSON["eventTitle"]);
            //$notifModalWrap.fadeIn(500);


            $(".alertTitle").text(curEventTitle);

            $(".alertDay").text(dayString);
            $(".alertTime").text(curTimeString);
            $(".notificationModal.alert").fadeIn();
            $(".blackShade").fadeIn();

            // Speak notification
            //var name = localStorage.getItem("firstName");
            //var phrase = "Hello " + name + ". Don't forget. " + curEventTitle + " is at " + curEventDate.toString();
            //speakPhrase(phrase);
        });

        // Show Notification Modal on receive
        cordova.plugins.notification.local.on("schedule", function(notification) {
            //alert("triggered: " + notification.id);
            //var dataJSON = JSON.parse(notification.data);
            //
            ////alert(dataJSON["eventTime"]);
            //
            //var datetimeArr = datetimeFormat(dataJSON["eventTime"]);    // [time, date]
            //var curEventID = dataJSON["eventID"];
            //var curEventTitle = dataJSON["eventTitle"];
            //
            //var curTime = datetimeArr["time"];          // 03:05:00
            //var curTimeString = timeFormat(curTime);    // 3:05 AM
            //
            //var curDate = datetimeArr["date"];          // 2015-20-12
            //
            //var curDateFormatArr = dateFormat(curDate); // [year, month, monthName, day, dateLine]
            //
            //var dayString = curDateFormatArr["dayName"];
            //
            //if (getCurDateString() == curDateFormatArr["dateLine"]) {
            //    dayString = "Today";
            //}
            //
            //alert(dayString);
        });
    }

    function receivedAllRemindersOfPatient(data) {

        // Clear All notifications
        cordova.plugins.notification.local.clearAll(function() {
            //alert("cleared All notifications");
        }, this);

        // TODO need to add month's to value
        var timeUnitMap = {'Minutes' : 60*1000,
            'Hours': 60*60*1000,
            'Days': 24*60*60*1000
        };

        for (var i = 0; i < data.length; i++) {
            var reminder = data[i];

            // Get "EventID", "EventTitle" and "EventTime"
            var evID = reminder["EventID"];
            var eventTime = reminder["EventTime"];
            var eventTitle = reminder["EventTitle"];

            // Get Reminder quantity
            var quantityNum = reminder["QuantityNum"];  // 5 or 10 units of time measure

            // Get ReminderType and Unit i.e. 'Minutes' => 60000 ms
            var remTypeName = reminder["ReminderTypeName"]; // Minutes/ Hours / Days
            var remTypeMilli = timeUnitMap[remTypeName];        // i.e. 60*1000

            // get EventTime in milliseconds
            var eventTimeObj = dateTimeToDateObj(eventTime);
            var eventTimeMilli = eventTimeObj.getTime();   // get Datetime in milliseconds

            // get datetime of Reminder by subtracking EventTime - quant
            var reminderTime = new Date(eventTimeMilli - (quantityNum * remTypeMilli));

            var remID = reminder["ReminderID"];

            //alert(eventTimeObj.toString() + "  vs  " + reminderTime.toString());
            scheduleNotification(remID, reminderTime, evID, eventTitle, eventTime);
        }
    }

    function scheduleNotification(remID, remTime, evID, evTitle, evTime) {
        // Notifications

        // Testing purpose if want to see a reminder
        //var now             = new Date().getTime(),
        //    _5_sec_from_now = new Date(now + 2*1000);
        //remTime = _5_sec_from_now;

        cordova.plugins.notification.local.schedule({
            id: remID,
            at: remTime,
            data: {
                eventID   : evID,
                eventTitle: evTitle,
                eventTime : evTime
            }
        });
    }

    function dateTimeToDateObj(dateTimeString) {
        // Split timestamp into [ Y, M, D, h, m, s ]
        var t = dateTimeString.split(/[- :]/);

        // Apply each element to the Date function
        var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

        return d;
    }

    $("#notifDismissBtn").on("touchend", function(){
        var $notifModalWrap = $("#notificationModalWrap");
        $notifModalWrap.fadeOut(300);
    });

// Check Firebase if received call
    function loadCallFirebase() {
        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patID = localStorage.getItem("patient");
        var patFire = fire.child(patID + "/callLog");

        patFire.on('child_added', fbCallAdded);
    }

// Receiving a call
    function fbCallAdded(snapshot){
        var msg = snapshot.val();

        // remove from call log if received
        clearCallLog();

        if (localStorage.getItem("callID") == null) {
            navWithParams("call.html");
        }


    }

// Clears the patients call log
    function clearCallLog(){
        // remove from call log if received
        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patID = localStorage.getItem("patient");
        var patFire = fire.child(patID + "/callLog");
        patFire.set(null);
    }


}());