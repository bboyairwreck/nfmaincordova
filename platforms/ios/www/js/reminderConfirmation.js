!(function(){
    var patID = localStorage.getItem("patient");

    var eventName = "";
    var eventDate = "";
    var eventTime = "";

    $(document).ready(function() {
        myParams = getParams();
        var eventDateString = "";
        var eventTimeString = "";

        if (myParams != null) {
            eventName = myParams["eventName"];
            eventDate = myParams["eventDate"];
            var eventDateStringArr = dateFormat(eventDate);
            eventDateString = eventDateStringArr["dateLine"];
            eventTime = myParams["eventTime"];
            eventTimeString = timeFormat(eventTime);
        }
        $("#details").html("<p>" + eventName + "</p><p>" + eventDateString + "</p><p>" + eventTimeString + "</p>");
    });

    $("#createReminder").on("touchend", function(){
        // ajax call to send in params
        var url = "http://ericchee.com/neverforgotten/addEventReminder.php";
        var datetime = eventDate + " " + eventTime;
        $.ajax(url, {
            dataType : "json",
            data : {
                'name': eventName,
                'time': datetime,
                'id' : patID
            },
            success : createSuccess,
            error : ajaxCreateError
        });

    });

    function createSuccess(data) {

        // get Firebase to Person we're calling
        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patID = localStorage.getItem("patient");
        var patCallLogFire = fire.child(patID + "/reminderCreated");

        var evID = data["EventID"];

        // push to firebase - send your (patient's) id along with w
        patCallLogFire.push({
            EventID : evID
        });

        params.pop();
        params.pop();
        var href = $("#createReminder").data("href");   // nav back to action.html
        navWithParams(href);
    }

    function ajaxCreateError( xhr, status, errorThrown ) {
        alert(errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }


    $("#backTime").click(function() {
        params.pop();
        var href = $(this).data("href");
        navWithParams(href);
    });
}());




